﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour
{
    public int m_backgroundSize;

    private Transform cameraTransform;
    private Transform[] layers;
    private float viewZone = 5;
    private int leftIndex;
    private int rightIndex;

    public float paralaxSpeed;
    private float lastCameraX;

	public void Initialize()
    {
        cameraTransform = Camera.main.transform;
        lastCameraX = cameraTransform.position.x;


        layers = new Transform[transform.childCount];
        for (int i = 0; i < transform.childCount; ++i)
        {
            layers[i] = transform.GetChild(i);
        }

        leftIndex = 0;
        rightIndex = layers.Length - 1;
	}

    private void ScrollLeft()
    {
        layers[rightIndex].position = Vector3.right * (layers[leftIndex].position.x - m_backgroundSize)
                                    + Vector3.up * layers[leftIndex].position.y;
        leftIndex = rightIndex;
        rightIndex--;
        if (rightIndex < 0)
            rightIndex = layers.Length - 1;
    }
    private void ScrollRight()
    {
        layers[leftIndex].position = Vector3.right * (layers[rightIndex].position.x + m_backgroundSize)
                                   + Vector3.up * layers[rightIndex].position.y;
        rightIndex = leftIndex;
        leftIndex++;
        if (leftIndex >= layers.Length)
            leftIndex = 0;
    }

	public void TickUpdate ()
    {
        float deltaX = cameraTransform.position.x - lastCameraX;
        transform.position += Vector3.right * (-deltaX * paralaxSpeed);
        lastCameraX = cameraTransform.position.x;

        if (cameraTransform.position.x < (layers[leftIndex].transform.position.x + viewZone))
            ScrollLeft();
        if (cameraTransform.position.x > (layers[rightIndex].transform.position.x - viewZone))
            ScrollRight();
    }
}
