﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelHandler : MonoBehaviour
{

    private float m_floorHeight;
    private GameObject m_background;
    private GameObject m_foreground;
    public List<GameObject> m_platformPrefabs; // set in unity editor

    public List<GameObject> m_platforms;

    // TODO: potentially load level from data file
    public void SetData(float floorHeight, GameObject background, GameObject foreground)
    {
        m_floorHeight = floorHeight;
        m_background = background;
        m_foreground = foreground;
        CreateRandomPlatforms();
    }

    private void CreateRandomPlatforms()
    {
        m_platforms = new List<GameObject>();
        for (int i = 0; i < 10; i++)
        {
            int idx = Random.Range(0, m_platformPrefabs.Count);
            GameObject go = Instantiate<GameObject>(m_platformPrefabs[idx]) as GameObject;
            m_platforms.Add(go);
            float randXOffset = Random.Range(5, 12);
            //float randYOffset = Random.Range(0, 2);
            go.transform.position = new Vector3(i * 8 + randXOffset, m_floorHeight + 0.5f/*randYOffset*/, 0);
                        
            LineRenderer lr = go.AddComponent<LineRenderer>();
            lr.material = new Material(Shader.Find("Particles/Additive"));
            lr.widthMultiplier = 0.1f;
            lr.positionCount = 5;
            lr.startColor = new Color(1, 0, 0, 1);
            lr.endColor = new Color(1, 0, 0, 1);
            lr.sortingLayerName = "maincharacter";
        }
    }

    
    public void updatePlatformLineRenderers()
    {
        foreach (GameObject platform in m_platforms)
        {
            RectTransform trans = platform.GetComponent<RectTransform>();
            Debug.Assert(trans != null);
            Vector3[] corners = new Vector3[4];
            trans.GetWorldCorners(corners);
            Vector3 bottomeLeft = corners[0];
            Vector3 topRight = corners[2];

            LineRenderer lr = platform.GetComponent<LineRenderer>();
            lr.SetPosition(0, new Vector3(bottomeLeft.x, bottomeLeft.y, -1f));
            lr.SetPosition(1, new Vector3(bottomeLeft.x, topRight.y, -1f));
            lr.SetPosition(2, new Vector3(topRight.x, topRight.y, -1f));
            lr.SetPosition(3, new Vector3(topRight.x, bottomeLeft.y, -1f));
            lr.SetPosition(4, new Vector3(bottomeLeft.x, bottomeLeft.y, -1f));
        }
    }


    public void TickUpdate()
    {
        m_background.GetComponent<ScrollingBackground>().TickUpdate();
        m_foreground.GetComponent<ScrollingBackground>().TickUpdate();
    }


}
