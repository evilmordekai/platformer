﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goodguy : MonoBehaviour {

    public float speed;
    public Transform foreground;
    public GameManager gameManager;

    public enum PlayerState { Idle = 0, Attack, Run, Crouch, Hurt, Jump};

    string _currentDirection = "right";
    public PlayerState _currentAnimationState = PlayerState.Idle;
    Animator m_myAnimator;
    private LineRenderer m_lineRenderer = null;

    private Animator GetAnimator()
    {
        if (m_myAnimator != null) return m_myAnimator;

        // this will return NULL inexplicably (even when it was previously not null)... what the actual fuck.
        m_myAnimator = this.GetComponent<Animator>();
        return m_myAnimator;
    }

    void Start()
    {
        m_lineRenderer = gameObject.AddComponent<LineRenderer>();
        m_lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
        m_lineRenderer.widthMultiplier = 0.1f;
        m_lineRenderer.positionCount = 5;
        m_lineRenderer.startColor = new Color(1, 0, 0, 1);
        m_lineRenderer.endColor = new Color(1, 0, 0, 1);
        m_lineRenderer.sortingLayerName = "maincharacter";
    }

    public static Vector2 s_GRAVITY = new Vector2(0.0f, -9.8f);
    public bool m_isOnGround = true;
    public bool m_isCrouching = false; 
    public static float s_GROUND_FRICTION = 0.9f;
    public static float s_AIR_FRICTION = 0.99f;
    public Vector2 m_velocity;

    public void updateLineRenderer()
    {
        RectTransform trans = GetComponent<RectTransform>();
        Debug.Assert(trans != null);
        Vector3[] corners = new Vector3[4];
        trans.GetWorldCorners(corners);
        Vector3 bottomeLeft = corners[0];
        Vector3 topRight = corners[2];

        m_lineRenderer.SetPosition(0, new Vector3(bottomeLeft.x, bottomeLeft.y, -1f));
        m_lineRenderer.SetPosition(1, new Vector3(bottomeLeft.x, topRight.y, -1f));
        m_lineRenderer.SetPosition(2, new Vector3(topRight.x, topRight.y, -1f));
        m_lineRenderer.SetPosition(3, new Vector3(topRight.x, bottomeLeft.y, -1f));
        m_lineRenderer.SetPosition(4, new Vector3(bottomeLeft.x, bottomeLeft.y, -1f));
    }

    private void ApplyAcceleration(Vector2 acceleration)
    {
        m_velocity.y += acceleration.y;
    }
    private void UpdateGravity(float delta)
    {
        if (m_isOnGround)
        {
            //velocity.y *= 1.0f - ((1.0f - s_GROUND_FRICTION) * delta);
            m_velocity.y = 0;
        }
        else
        {
            m_velocity.y *= 1.0f - ((1.0f - s_AIR_FRICTION) * delta);
        }

        Vector3 newPosition = new Vector3(transform.position.x, transform.position.y + (m_velocity.y * delta), 0);
        //change position by velocity
        this.transform.position = newPosition;
    }

    public void SetGrounded(bool value)
    {
        m_isOnGround = value;
        if (m_isOnGround)
        {
            m_isJumping = false;
            m_jumpButtonOnCooldown = false;
        }
    }

    public bool m_isHurtAndInvincible = false;
    private bool m_jumpButtonOnCooldown = false;
    private IEnumerator HandleJumpCooldown()
    {
        m_jumpButtonOnCooldown = true;
        yield return new WaitForSeconds(1.0f);
        m_jumpButtonOnCooldown = false;
    }

    public bool m_isAttacking = false;
    public bool m_isJumping = false;
    public bool m_isOnPlatform = false;
    public bool m_platformCollisionsEnabled = true;
    private IEnumerator RenenablePlatformCollisions()
    {
        yield return new WaitForSeconds(1.0f);
        m_platformCollisionsEnabled = true;
    }
    static long s_debugFrameCounter = 0;
    void Update()
    {
        ++s_debugFrameCounter;
        bool inputDetected = false;
        float horizTranslation = Input.GetAxis("Horizontal");
        float vertTranslation = Input.GetAxis("Vertical");
        float distance = speed * Time.deltaTime;
        m_isCrouching = false;
        bool isRunning = false;

        Animator anim = this.GetAnimator();
        if (anim)
        {
            AnimatorStateInfo state = anim.GetCurrentAnimatorStateInfo(0);
            if (state.IsName("hero_attack") ||
                state.IsName("hero_jump_and_attack") ||
                state.IsName("hero_crouch_and_slash"))
            {
                if (state.normalizedTime >= state.length)
                {
                    m_isAttacking = false;
                }
            }
            else
                m_isAttacking = false;

        }

        if ( Input.GetKey(KeyCode.DownArrow) || vertTranslation > 0.50)
        {
            inputDetected = true;
            m_isCrouching = true;
            changeState(PlayerState.Crouch);
        }

        if (!m_isHurtAndInvincible)
        {
            if (!m_isCrouching ||
                (!m_isOnGround && !m_isOnPlatform))
            {
                if (Input.GetKey(KeyCode.LeftArrow) || horizTranslation < -0.5)
                {
                    isRunning = true;
                    inputDetected = true;
                    changeDirection("left");
                    this.transform.position += Vector3.left * distance;
                }
                else if (Input.GetKey(KeyCode.RightArrow) || horizTranslation > 0.5)
                {
                    isRunning = true;
                    inputDetected = true;
                    changeDirection("right");
                    this.transform.position += Vector3.right * distance;
                }
            }
        }

        if (!m_isHurtAndInvincible && !m_isAttacking && (Input.GetKey(KeyCode.A) || Input.GetButtonDown("Fire1")))
        {
            if(m_isCrouching && !m_isOnGround && !m_isOnPlatform) // striaght/quick dive/attack downward
            {
                ApplyAcceleration(new Vector2(0, -8.0f));
            }
            m_isAttacking = true;
            inputDetected = true;
            changeState(PlayerState.Attack);
        }

        Vector2 frameGravity = new Vector2(s_GRAVITY.x * Time.deltaTime, s_GRAVITY.y * Time.deltaTime);
        ApplyAcceleration(frameGravity);

        if (!m_isHurtAndInvincible && !m_jumpButtonOnCooldown && 
            (Input.GetKey(KeyCode.Space) || Input.GetButtonDown("JumpJ")))
        {
            inputDetected = true;
            if (m_isCrouching && m_isOnPlatform)
            {
                // fall through a platform
                m_platformCollisionsEnabled = false;
                ApplyAcceleration(new Vector2(0, -5.0f));
                changeState(PlayerState.Jump);
                StartCoroutine(RenenablePlatformCollisions());
            }
            else
            {
                ApplyAcceleration(new Vector2(0, 6.2f));
                changeState(PlayerState.Jump);
                m_isJumping = true;
            }
            SetGrounded(false);
            StartCoroutine(HandleJumpCooldown());
        }
        if(!m_isHurtAndInvincible && isRunning && !m_isJumping)
            changeState(PlayerState.Run);

        UpdateGravity(Time.deltaTime);

        // nothing being pressed, go to Idle state
        if (!m_isHurtAndInvincible && 
            !m_isJumping && 
            !m_isAttacking &&
            !inputDetected && 
            (m_isOnPlatform || m_isOnGround))
        {
            changeState(PlayerState.Idle);
        }
    }

    public IEnumerator Unhurt()
    {
        yield return new WaitForSeconds(0.5f);
        changeState(PlayerState.Idle);
        m_isHurtAndInvincible = false;
    }
    public void SetHurt()
    {
        changeState(PlayerState.Hurt);
        m_isHurtAndInvincible = true;
        StartCoroutine(Unhurt());
    }

    void changeState(PlayerState state)
    {
        switch (state)
        {
            case PlayerState.Idle:
            case PlayerState.Run:
            case PlayerState.Attack:
            case PlayerState.Crouch:
            case PlayerState.Jump:
            case PlayerState.Hurt:
                Animator anim = GetAnimator();
                if(anim)
                    anim.SetInteger("state", (int)state);
                _currentAnimationState = state;
                break;
            default: Debug.Assert(false, "unkown player state");
                break;
        }
    }

    void changeDirection(string direction)
    {
        SpriteRenderer sr = this.GetComponent<SpriteRenderer>();
        // apparently the gamemanager is calling this function because it has a goodguys script attached to it.  i'm obviously doing it wrong.
        if (sr)
        {
            if (_currentDirection != direction)
            {
                if (direction == "right")
                {
                    sr.flipX = false;
                    _currentDirection = "right";
                }
                else if (direction == "left")
                {
                    sr.flipX = true;
                    _currentDirection = "left";
                }
            }
        }
    }


}
