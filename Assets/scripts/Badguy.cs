﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Badguy : MonoBehaviour {

    public GameObject goodguy;
    public float m_speed;
    private LineRenderer m_lineRenderer = null;

    void Start()
    {
        m_lineRenderer = gameObject.AddComponent<LineRenderer>();
        if (m_lineRenderer)
        {
            m_lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
            m_lineRenderer.widthMultiplier = 0.1f;
            m_lineRenderer.positionCount = 5;
            m_lineRenderer.startColor = new Color(1, 0, 0, 1);
            m_lineRenderer.endColor = new Color(1, 0, 0, 1);
            m_lineRenderer.sortingLayerName = "maincharacter";
        }
        StartCoroutine(SwitchDirections());
    }

    public void updateLineRenderer()
    {
        RectTransform trans = GetComponent<RectTransform>();
        Debug.Assert(trans != null);
        Vector3[] corners = new Vector3[4];
        trans.GetWorldCorners(corners);
        Vector3 bottomeLeft = corners[0];
        Vector3 topRight = corners[2];

        m_lineRenderer.SetPosition(0, new Vector3(bottomeLeft.x, bottomeLeft.y, -1f));
        m_lineRenderer.SetPosition(1, new Vector3(bottomeLeft.x, topRight.y, -1f));
        m_lineRenderer.SetPosition(2, new Vector3(topRight.x, topRight.y, -1f));
        m_lineRenderer.SetPosition(3, new Vector3(topRight.x, bottomeLeft.y, -1f));
        m_lineRenderer.SetPosition(4, new Vector3(bottomeLeft.x, bottomeLeft.y, -1f));
    }

    private Vector3 direction = Vector3.left;
    void Update()
    {

        float distance = m_speed * Time.deltaTime;
        transform.position += direction * distance;
    }


    private IEnumerator SwitchDirections()
    {
        yield return new WaitForSeconds(Random.Range(2, 10));

        if (direction == Vector3.left)
            direction = Vector3.right;
        else
            direction = Vector3.left;
        StartCoroutine(SwitchDirections());
    }
}
