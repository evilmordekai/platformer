﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public GameObject m_hero;  // initially set in unity editor, overriden at runtime
    public GameObject badguyPrefab;  // set in unity editor
    public GameObject m_camera;
    public List<GameObject> m_enemies;
    public float floorHeight;
    public GameObject background;
    public GameObject foreground;


    void Start()
    {
        InitLevel(); 
    }

    private void InitLevel()
    {
        StopCoroutine(SpawnBadGuys());


        // background & foreground
        {
            GameObject go = Instantiate(background) as GameObject;
            go.name = "background";
            background = go;
            background.GetComponent<ScrollingBackground>().Initialize();

            GameObject go2 = Instantiate(foreground) as GameObject;
            go2.name = "foreground";
            foreground = go2;
            foreground.GetComponent<ScrollingBackground>().Initialize();
        }


        // hero
        {
            GameObject go = Instantiate(m_hero) as GameObject;
            go.transform.position = new Vector3(1, floorHeight, 0);
            go.name = "HERO";
            m_hero = go;

            SpriteRenderer sr = go.GetComponent<SpriteRenderer>();
            sr.enabled = true;

            Goodguy hs = go.GetComponent<Goodguy>();
            hs.gameManager = this;
        }

        StartCoroutine(SpawnBadGuys());
        this.GetComponentInParent<LevelHandler>().SetData(floorHeight, background, foreground);
    }

    private IEnumerator SpawnBadGuys()
    {
        while (true)
        {
            yield return new WaitForSeconds(2.0f);

            GameObject go = Instantiate(badguyPrefab) as GameObject;
            go.transform.position = new Vector3(m_hero.transform.position.x + 30,  /*TODO: randomize to drop onto a ledge*/floorHeight, 0);
            go.name = "FROWNIE FACE";
            go.GetComponent<Badguy>().m_speed = 4;
            go.GetComponent<SpriteRenderer>().enabled = true;
            m_enemies.Add(go);

            Badguy bgm = go.GetComponent<Badguy>();
            bgm.goodguy = m_hero;

            SpriteRenderer sr = go.GetComponent<SpriteRenderer>();
            sr.enabled = true;
        }
    }
    
    void Update()
    {
        float heroX = m_hero.transform.position.x + 0.75f;
        float heroY = m_hero.transform.position.y + 0.75f;
        m_camera.transform.position = 
            new Vector3(heroX, m_camera.transform.position.y, m_camera.transform.position.z);

        LevelHandler lr = this.GetComponentInParent<LevelHandler>();
        lr.TickUpdate();

        CheckEnemyCollisions();
        CheckPlatformCollisions();
        foreach (GameObject badguy in m_enemies)
        {
            badguy.GetComponent<Badguy>().updateLineRenderer();
        }


        m_hero.GetComponent<Goodguy>().updateLineRenderer();
        lr.updatePlatformLineRenderers();
    }

    private void CheckEnemyCollisions()
    {
        Goodguy goodguyScript = m_hero.GetComponent<Goodguy>();
        List<GameObject> toBeRemoved = new List<GameObject>();
        foreach (GameObject go in m_enemies)
        {
            if (Collides(m_hero.gameObject, go))
            {
                //if (goodguyScript.m_isAttacking) // TODO: FACING creature during the attack
                {
                    go.GetComponent<SpriteRenderer>().enabled = false;
                    toBeRemoved.Add(go); // TODO - death animation/state
                }
                //else if(!goodguyScript.m_isHurtAndInvincible)
                //    goodguyScript.SetHurt();
            }
        }

        foreach (GameObject deleteMe in toBeRemoved)
        {
            m_enemies.Remove(deleteMe);
            Destroy(deleteMe);
        }
    }

    private void CheckPlatformCollisions()
    {
        Goodguy goodGuyScript = m_hero.GetComponent<Goodguy>();
        

        // if we're not jumping
        if (goodGuyScript.m_velocity.y <= 0)
        {
            bool landedOnPlatform = false;
            RectTransform heroTrans = m_hero.GetComponent<RectTransform>();
            if (goodGuyScript.m_velocity.y < 0 || goodGuyScript.m_isOnPlatform)
            {
                if (goodGuyScript.m_platformCollisionsEnabled)
                {
                    foreach (GameObject platform in this.GetComponentInParent<LevelHandler>().m_platforms)
                    {
                        Vector3[] heroCorners = new Vector3[4];
                        heroTrans.GetWorldCorners(heroCorners);
                        Vector3 heroBottomLeft = heroCorners[0];
                        Vector3 heroTopRight = heroCorners[2];

                        Vector3[] platformCorners = new Vector3[4];
                        platform.GetComponent<RectTransform>().GetWorldCorners(platformCorners);
                        Vector3 platformBottomLeft = platformCorners[0];
                        Vector3 platformTopRight = platformCorners[2];

                        // the sprite frames are not all the same size and the transform seems to be incuslive of all of them.
                        //    which means the characters feet do not line up with the bottom of the transform.  we make an attempt at adjusting it here manually 
                        //     additionally, there is plenty of 'void space' between the actual character and the two far-sides of the transform
                        double heroWidth = Math.Abs(heroTopRight.x - heroBottomLeft.x);
                        double heroHeight = Math.Abs(heroBottomLeft.y - heroTopRight.y);
                        double heroPseudoBottomTransform = heroBottomLeft.y + heroHeight * 0.3;
                        double heroPseudoLeftTransform = heroBottomLeft.x + heroWidth * 0.3;
                        double heroPseudoRightTransform = heroTopRight.x - heroWidth * 0.3;
                        double heroPseudoTopTransform = heroTopRight.y - heroHeight * 0.3;

                        if (heroPseudoLeftTransform > platformBottomLeft.x &&
                           heroPseudoRightTransform < platformTopRight.x &&
                           heroPseudoBottomTransform <= platformTopRight.y &&
                           heroPseudoTopTransform > platformTopRight.y)
                        {
                            landedOnPlatform = true;
                            goodGuyScript.m_isOnPlatform = true;
                            goodGuyScript.SetGrounded(true);

                            LineRenderer lr = platform.GetComponent<LineRenderer>();
                            lr.startColor = new Color(0, 1, 0, 1);
                            lr.endColor = new Color(0, 1, 0, 1);
                        }
                    }
                }
                if (!landedOnPlatform)
                    goodGuyScript.m_isOnPlatform = false;
            }
            if (!goodGuyScript.m_isOnPlatform && m_hero.transform.position.y > floorHeight)
                goodGuyScript.SetGrounded(false);

            if (m_hero.transform.position.y <= floorHeight)
                goodGuyScript.SetGrounded(true);
        }
}

    bool Collides(GameObject obj, GameObject item)
    {
        Debug.Assert(obj != null);
        Debug.Assert(item != null);

        RectTransform themTrans = item.GetComponent<RectTransform>();
        Debug.Assert(themTrans != null);
        Vector3[] themCorners = new Vector3[4];
        themTrans.GetWorldCorners(themCorners);
        Vector3 frownBottomLeft = themCorners[0];
        Vector3 frownTopRight = themCorners[2];

        RectTransform smileTrans = obj.GetComponent<RectTransform>();
        Debug.Assert(smileTrans != null);
        Vector3[] smileCorners = new Vector3[4];
        smileTrans.GetWorldCorners(smileCorners);
        Vector3 smileBottomLeft = smileCorners[0];
        Vector3 smileTopRight = smileCorners[2];
        
        if (smileBottomLeft.x > frownTopRight.x ||
            smileBottomLeft.y > frownTopRight.y ||
            smileTopRight.x < frownBottomLeft.x ||
            smileTopRight.y < frownBottomLeft.y)
        {
            return false;
        }
        return true;
    }
}
