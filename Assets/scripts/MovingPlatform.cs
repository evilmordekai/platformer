﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {

    public float m_speed;

    void Start()
    {
        StartCoroutine(SwitchDirections());
    }
    

    private IEnumerator SwitchDirections()
    {
        yield return new WaitForSeconds(4.0f);

        if (direction == Vector3.left)
            direction = Vector3.right;
        else
            direction = Vector3.left;
        StartCoroutine(SwitchDirections());
    }

    private Vector3 direction = Vector3.left;
    void Update()
    {
        float distance = m_speed * Time.deltaTime;
        transform.position += direction * distance;
    }
}
